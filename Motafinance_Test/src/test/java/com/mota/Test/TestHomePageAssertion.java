package com.mota.Test;

import org.testng.annotations.Test;

import com.mota.data.Apply1_data;
import com.mota.data.Apply2_data;
import com.mota.utilities.ApplyPage_Util;
import com.mota.utilities.AssertHomePage_Util;
import com.mota.utilities.Apply2_Util;
import com.mota.utilities.SetupUtil;

public class TestHomePageAssertion extends SetupUtil {

	@Test
	public void TestHomePageassertion() {
		AssertHomePage_Util homepage = new AssertHomePage_Util(driver);

		getURL("https://dev.motafinance.co.uk/");
		homepage.Assert_Home_Page_Logo();
		homepage.Assert_Login_Btn_Upper();
		homepage.Assert_Apply_Btn_Upper();
		homepage.Assert_Page_About_Us();
		homepage.Assert_Why_MotaFinance();
		homepage.Assert_How_It_Works();
		homepage.Assert_Faq();
		homepage.Assert_Car_Loans();
		homepage.Assert_Contact();

	}

}
