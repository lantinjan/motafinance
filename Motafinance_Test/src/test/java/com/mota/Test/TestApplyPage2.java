package com.mota.Test;

import org.testng.annotations.Test;

import com.mota.data.Apply1_data;
import com.mota.data.Apply2_data;
import com.mota.utilities.ApplyPage_Util;
import com.mota.utilities.Apply2_Util;
import com.mota.utilities.SetupUtil;

public class TestApplyPage2 extends SetupUtil{
	@Test
	public void TestApplypage1() {

		ApplyPage_Util apply = new ApplyPage_Util(driver);
		Apply1_data data = new Apply1_data();
		Apply2_data data2 = new Apply2_data();

		apply.Apply1_URL(data.devURL); 
		apply.ApplyNow();
		apply.selectTitle(data.titleMr);
		apply.EnterFirstName(data.Fname);
		apply.EnterLastName(data.Lname);
		apply.EnterEmail(data.email);
		apply.EnterPhone(data.phone);
		apply.EnterBirthday(); 
		apply.EnterPassword(data.password);
		apply.chkPolicy(); 
		apply.chkbox2();
		apply.ClickContinue();
		apply.enterPostcode(data2.postcode);
		apply.enterHouseNumber(data2.house_number);
		apply.enterHouseName(data2.house_name);
		apply.enterStreet(data2.Street);
		apply.enterCityTown(data2.CityTown);
		apply.selectHouseStatus(data2.HomeStatus);
		apply.enterDateMoveMth(data2.DateMoveMth);
		apply.enterHouseStatus(data2.DateMoveYear); 
		apply.ClickContinue();
		 
		
		
}

}
