package com.mota.utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.mota.data.Apply1_elements;
import com.mota.data.Apply2_elements;
import com.mota.library.FunctionReference;

public class Apply2_Util extends FunctionReference
{
	
	public Apply2_Util(WebDriver driver) {
		super(driver);
	}
	Apply2_elements dataElements= new Apply2_elements();
	
	public void enterPostcode(String postcode)	{ 
		type(By.xpath(dataElements.postcode), postcode);
	}
	public void enterHouseNumber(String housenumber)	{
		type(By.xpath(dataElements.house_number), housenumber);
	}
	public void enterHouseName(String housename)	{
		type(By.xpath(dataElements.house_name), housename);
	}
	public void enterStreet(String street)	{
		type(By.xpath(dataElements.Street), street);
	}
	public void enterCityTown(String citytown)	{
		type(By.xpath(dataElements.house_number), citytown);
	}
	public void selectHouseStatus(String housestatus)	{
		selectFromDropdown(By.xpath(dataElements.HomeStatus),housestatus);
	}
	public void enterDateMoveMth(String movemth)	{
		selectFromDropdown(By.xpath(dataElements.DateMoveMth),movemth);
	}
	public void enterHouseStatus(String move_y)	{
		selectFromDropdown(By.xpath(dataElements.DateMoveYear),move_y);
	}
	
	
	
	

}
