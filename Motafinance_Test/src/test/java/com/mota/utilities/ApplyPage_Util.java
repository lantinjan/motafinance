package com.mota.utilities;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.mota.library.FunctionReference;
import com.mota.data.Apply1_elements;
import com.mota.data.Apply2_elements;
import com.mota.data.Apply3_elements;
import com.mota.data.Apply1_data;

public class ApplyPage_Util extends FunctionReference {
	
	public ApplyPage_Util(WebDriver driver) {
		super(driver);
	}
	Apply1_elements dataElements= new Apply1_elements();
	Apply2_elements dataElements2= new Apply2_elements();
	Apply3_elements dataElements3= new Apply3_elements();
	
	public void Apply1_URL(String Url)
	{
		driver.get(Url);
	}
	
	public void ApplyNow () {
		
		click(By.xpath(dataElements.ApplyNow));
		
	}
	public void selectTitle(String title)
	{ 
		selectFromDropdown(By.xpath(dataElements.Title),title);
	}
	public void EnterFirstName(String fname) {
		
		type(By.xpath(dataElements.fname), fname);
	} 
	public void EnterLastName(String lastname) {
		
		type(By.xpath(dataElements.lname),lastname);
	}
	public void EnterEmail(String email) {
		
		type(By.xpath(dataElements.email),email);
	}
	public void EnterPhone(String phone)
	{
		type(By.xpath(dataElements.mobile_phone),phone);
	}
	public void EnterPassword(String pass)
	{
		type(By.xpath(dataElements.password),pass);
	}
	public void EnterBirthday()
	{
		
		click(By.xpath(dataElements.bdayField));
		
		for(int i=0;i<=2;i++)
    	{
    		click(By.xpath(dataElements.bdayChooseYear));
    	}
		click(By.xpath(dataElements.bdaySelectYear));
		click(By.xpath(dataElements.bdaySelectMth));
		click(By.xpath(dataElements.bdaySelectDay));
	}
	public void chkPolicy(){
		
		scroll(By.xpath(dataElements.chkboxPolicy));
		click(By.xpath(dataElements.chkboxPolicy));

	}
	public void chkbox2()
	{
		scroll(By.xpath(dataElements.chkbox2));
		click(By.xpath(dataElements.chkbox2));
	}
	public void ClickContinue()
	{
		try {
			click(By.xpath(dataElements.app1continue)); 
		}
		catch(Exception b)
		{
			b.printStackTrace();
		}
	}
	public void enterPostcode(String postcode)	{
		type(By.xpath(dataElements2.postcode), postcode);
	}
	public void enterHouseNumber(String housenumber)	{
		type(By.xpath(dataElements2.house_number), housenumber);
	}
	public void enterHouseName(String housename)	{
		type(By.xpath(dataElements2.house_name), housename);
	}
	public void enterStreet(String street)	{
		type(By.xpath(dataElements2.Street), street);
	}
	public void enterCityTown(String citytown)	{
		type(By.xpath(dataElements2.CityTown), citytown);
	}
	public void selectHouseStatus(String housestatus)	{
		selectFromDropdown(By.xpath(dataElements2.HomeStatus),housestatus);
	}
	public void enterDateMoveMth(String movemth)	{
		selectFromDropdown(By.xpath(dataElements2.DateMoveMth),movemth);
	}
	public void enterHouseStatus(String move_y)	{
		selectFromDropdown(By.xpath(dataElements2.DateMoveYear),move_y);
	}
	public void enterEmployer(String emp)	{
		type(By.xpath(dataElements3.employersname), emp);
	}
	public void enterJobDetails(String jTitle)	{
		type(By.xpath(dataElements3.jobTitle), jTitle);
	}
	public void SelectEmpStatus(String empstat)	{
		selectFromDropdown(By.xpath(dataElements3.empStatus),empstat);
	}
	public void SelectOftenPaid(String oftenPaid)	{
		selectFromDropdown(By.xpath(dataElements3.oftenPaid),oftenPaid);
	}
	public void inputIncome(int netincome)	{
		typenum(By.xpath(dataElements3.netIncome),netincome);
	}
	public void SelectDateStartMth(String month)	{
		selectFromDropdown(By.xpath(dataElements3.DateStartM),month);
	}
	public void SelectDateStartYear(String year)	{
		selectFromDropdown(By.xpath(dataElements3.DateStartY),year);
	}
	public void SelectPaidDeposited(String deposited)	{
		selectFromDropdown(By.xpath(dataElements3.paidDeposited),deposited);
	}
	public void SelectNextPayDate( )	{
		click(By.xpath(dataElements3.NextPaydate));
		
	}
	public void SelectPayDate( ){
		scroll(By.xpath(dataElements3.NextPaydate));
		int x=1;
		while(x<=6) {
			try {
				if(driver.findElement(By.xpath("//div[@class='datepicker-days']/table/tbody/tr["+x+"]/td[@class='new day'][contains(.,'1')]")).isDisplayed()) {
					click(By.xpath("//div[@class='datepicker-days']/table/tbody/tr["+x+"]/td[@class='new day'][contains(.,'1')]"));
					x=6; 
				}
			}
			catch(Exception e)
			{
				x++;	
			}
			x++;
		}
	}
}
		
		

	

