package com.mota.utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.mota.library.FunctionReference;

public class SetupUtil {

	protected WebDriver driver;

	@BeforeMethod
	public void Setup()
	{
	
		  ChromeOptions ChromeOptions = new ChromeOptions();
		 ChromeOptions.addArguments("--headless", "window-size=1024,768","--no-sandbox");
		
		driver = new ChromeDriver(ChromeOptions);
		driver.manage().window().maximize();
		
	}
	public void getURL(String URL) {
		driver.get(URL);
		
	}
	
	/*@AfterMethod
	public void Close()
	{
		driver.quit();
	}*/
	
}
