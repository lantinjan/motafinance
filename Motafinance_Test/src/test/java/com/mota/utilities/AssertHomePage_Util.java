package com.mota.utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mota.data.HomePage_Elements;
import com.mota.data.HomePage_data;
import com.mota.library.FunctionReference;

public class AssertHomePage_Util extends FunctionReference {

	public AssertHomePage_Util(WebDriver driver) {
		super(driver);
	}

	HomePage_data data = new HomePage_data();
	HomePage_Elements dataElements = new HomePage_Elements();

	public void Assert_Home_Page_Logo() {
		getLogo(dataElements.imageLogo);
	}

	public void Assert_Login_Btn_Upper() {
		getButton(dataElements.login);
	}

	public void Assert_Apply_Btn_Upper() {
		getButton(dataElements.apply);
	}

	public void Assert_Page_About_Us() {
		String value = getText(By.xpath(dataElements.aboutUs));
		assertEquals(value, data.aboutus);
	}

	public void Assert_Why_MotaFinance() {
		String value = getText(By.xpath(dataElements.ymota));
		assertEquals(value, data.ymota);
	}

	public void Assert_How_It_Works() {
		String value = getText(By.xpath(dataElements.howitworks));
		assertEquals(value, data.howitworks);
	}

	public void Assert_Faq() {
		String value = getText(By.xpath(dataElements.faq));
		assertEquals(value, data.faq);
	}

	public void Assert_Car_Loans() {
		String value = getText(By.xpath(dataElements.carloans));
		assertEquals(value, data.carloans);
	}

	public void Assert_Contact() {
		String value = getText(By.xpath(dataElements.contact));
		assertEquals(value, data.contact);
	}
}
