package com.mota.library;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class FunctionReference {

	protected WebDriver driver;

	public FunctionReference(WebDriver driver) {

		this.driver = driver;

	}

	protected void openURL(String URL) {
		driver.get(URL);
	}

	protected void type(By locator, String value) {
		driver.findElement(locator).clear();
		driver.findElement(locator).sendKeys(value);
	}

	protected void typenum(By locator, int value) {
		driver.findElement(locator).sendKeys(String.valueOf(value));
	}

	protected void click(By locator) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(locator));
			element.click();
		} catch (Exception e) {
			WebElement select = driver.findElement(locator);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", select);
		}

	}

	public String getText(By locator) {
		String value = driver.findElement(locator).getText();
		return value;
	}

	protected void assertEquals(String currentVal, String correctVal) {
		try {
		Assert.assertEquals(currentVal, correctVal);
			System.out.println("PASSED: "+correctVal + " exist.");
		}catch (Exception e) {
			System.out.println("FAILED: "+correctVal + " doesn't exist.");
		}
	}

	protected void selectFromDropdown(By locator, String value) {
		Select dd = new Select(driver.findElement(locator));
		dd.selectByVisibleText(value);
	}

	protected void scroll(By locator) {
		WebElement selectScroll = driver.findElement(locator);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", selectScroll);
	}

	public void getLogo(String locator) {
		WebElement ImageFile = driver.findElement(By.xpath(locator));
		Boolean ImagePresent = (Boolean) ((JavascriptExecutor) driver).executeScript(
				"return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0",
				ImageFile);
		if (!ImagePresent) {
			System.out.println("FAILED: MotoFinance Logo not displayed.");
		} else {
			System.out.println("PASSED: MotaFinance Logo displayed.");
		}

	}

	public void getButton(String locator) {
		try {
			WebElement button = driver.findElement(By.xpath(locator));
			System.out.println("PASSED: Button exist enabled:" + button.isEnabled());
		} catch (Exception e) {
			System.out.println("FAILED: Button does not exist.");
		}
	}

}
