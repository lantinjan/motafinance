package com.mota.data;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.mota.library.FunctionReference;

public class HomePage_Elements {

	public static String imageLogo = "//*[@id=\"index\"]/header/div/div/div/div/a/img";
	public static String login = "//*[@id=\"index\"]/header/div/div/div/ul/li[1]/a";
	public static String apply = "//*[@id=\"index\"]/header/div/div/div/ul/li[2]/a";
	public static String aboutUs = "//*[@id=\"index\"]/header/nav/div/div/div/ul/li[1]/a";
	public static String ymota = "//*[@id=\"index\"]/header/nav/div/div/div/ul/li[2]/a";
	public static String howitworks = "//*[@id=\"index\"]/header/nav/div/div/div/ul/li[3]/a";
	public static String faq = "//*[@id=\"index\"]/header/nav/div/div/div/ul/li[4]/a";
	public static String carloans = "//*[@id=\"index\"]/header/nav/div/div/div/ul/li[5]/a";
	public static String contact = "//*[@id=\"index\"]/header/nav/div/div/div/ul/li[6]/a";

}
